## Installation
Since the app don't have any external dependencies - installation is a piece of cake.

Install node (currently using version 0.10.25)

```bash
$ sudo apt-get install node
```
Install npm
```bash
$ sudo apt-get install npm
```
Install the app
```bash
$ npm install git+https://git@bitbucket.org/lykata/crude.git --save
```
Wait did I say the application had no dependencies? Oh thats right, to run the tests you need mocha
```bash
$ npm install mocha -g
```

## Quickstart

Run tests to make sure everything is fine
```bash
$ mocha
```
Then start the app
```bash
$ node index.js
```
To make requests to the server, you can use curl
```bash
$ sudo apt-get install curl
```
Post a user named testuser
```bash
$ curl -H "Content-Type: application/json" -X POST -d '{"username":"testuser"}' http://localhost:1332/users
```
Get all users
```bash
curl http://localhost:1332/users
```
Get testuser
```bash
curl http://localhost:1332/users/testuser
```

## What is Crude?
Crude is an simple webbapp that supports adding and getting users. 
It is so simple that a user only consists of a username that _should_ be unique. 
Unfortunately there is no constraint for creating users with duplicate names in its current state,
but it is planned to be added in the future.

The idea is that the app should be simple and preferrably have no external dependencies at all.
At this moment, there is only one dependency which is used for testing - Mocha. However, it might be removed in future.

The webapp consists of three different layers. The layers are as follow:

1. The app
1. The webbapp framework
1. The database


## The app (server.js)
The app consists of a http-api and a backend-module that takes care of the business-logic.
The app allows adding and getting users from the database through HTTP-requests.

## The webbapp framework (crude.js)
The webbapp framework is built on top of nodejs core http-server.
At this moment, the provided helper-functions for handling HTTP-methods
mostly works as aliases for a particular url.

For instance:
```javascript
app.get(url,function(req, res){ //do stuff });
```
is equivalent with:
```javascript
app.get = function(url, function(res,req){ //do stuff });
if (url == req.url){
    app.get(res,req);
}
```

However, an exception to the rule is the method POST. If you receive a POST-request
from a client with content-type 'application/json', the web framework will 
do the JSON-parsing for you. That is, you will receive a JSON-object instead of
a JSON-string.

## The database (collections.js)
The database is not much of an database really. Right now it consists of a
collection object, that takes care of document IO-operations. The backend-module that is 
a part of the HTTP-api, uses the collection object directly for that purpose.

The current database state allows for asynchronous IO-operations. Beware -
file locking isn't implemented yet, so it might introduce some interesting bugs.
That's a feature that is on the high priority list.




