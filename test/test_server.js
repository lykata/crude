var http = require('http');
var server = require('../lib/server');
var assert = require('assert');
var fs = require('fs');
var path = require('path');

var PORT = 1332;
var HOST = 'localhost';

var default_post_option = {
    host: HOST,
    port: '1332',
    headers: {
        'Content-Type': 'application/json',
    }
};
var users = [
    {'username':' kalle'},
    {'username': 'birgit'},
    {'username': 'eva'}, 
    {'username': 'charles'}
];

describe('server', function() {
    before(function() {
        server.listen(PORT);
    });

    after(function() {
        server.close();
        var p = path.join(__dirname, 'users.json');
        fs.unlinkSync(p);
    });

    describe('/', function() {
        it('should return 200', function(done) {
            http.get('http://localhost:' + PORT, function(res) {
                assert.equal(200, res.statusCode);
                done();
            });
        });
    });
    describe('post /users ', function() {
        it('should return 201', function(done) {
            var options = default_post_option;
            var user = users[0];
            var data = JSON.stringify(user);
            options.method = 'POST';
            options.path = '/users';
            options.headers['Content-Length'] =  data.length;
            var req = http.request(options, function(res) {
                res.setEncoding('utf8');
                assert.equal(201, res.statusCode);
                done();
            });
            req.write(data);
            req.end();
        });
    });
    describe('get /users', function() {
        it('should return 200', function(done) {
            var url = 'http://localhost:' + PORT + '/users';
            http.get(url, function(res) {
                assert.equal(200, res.statusCode);
                done();
            });
        });
    });
});
