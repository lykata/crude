var assert = require('assert');
var util = require('util');
var Collection = require('../lib/collection');
var fs = require('fs');


var users = [
    {'username':' kalle'},
    {'username': 'birgit'},
    {'username': 'eva'}, 
    {'username': 'charles'}
];
var file = util.format('%s/_users.json', __dirname);

describe('collection', function() { 
    var t;
    before(function(){ 
        t = new Collection(file);
        t.empty();
    });
    after(function() {
        t.drop();

    });
    describe('insert and get data', function() {
        users.forEach(function(user) {
            it('successfully inserts and gets ' + user.username, function(done){
                return t.insertItem(user, function(err) {
                    return t.getItem({username:user.username}, function(cuser) {
                        assert.equal(user.username, cuser.username);
                        done();
                    });
                });
             });
        });

    });
});

