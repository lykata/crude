var fs = require('fs');
var _path = require('path');
var util = require('util');

module.exports = Collection;


function Collection(path) {
    this.path = path;
    this.name = _path.basename(path).split(".")[0];

    if (!fs.existsSync(this.path))
        fs.writeFileSync(this.path, JSON.stringify({}));

    this.getIds((function(err, ids){
        if (ids.length){
            this._last_id = Math.max.apply(null, ids);
        }else
            this._last_id = 0;
    }).bind(this));
}

Collection.prototype.empty = function() {
    fs.writeFileSync(this.path, JSON.stringify({}));
    this._last_id = 0;
};
Collection.prototype.drop = function() {
    fs.unlinkSync(this.path);
    this._last_id = 0;
};

Collection.prototype.getIds = function(cb) {
    this.read(function(err, data) {
        var keys = Object.keys(data);
        var ids  =[].map.call(keys, function(key){ 
            return parseInt(key);
        });
        cb(err, ids);
    });
};

Collection.prototype.getNextId = function () {
    return parseInt(this._last_id + 1);
};

Collection.prototype.read = function (cb) {
    fs.readFile(this.path, function(err, data){
        var json = JSON.parse(data);
        return cb(err, json);
    });
};

Collection.prototype.insert = function (item, cb) {
    var json = JSON.stringify(item);
    fs.writeFile(this.path, json, function(err){
        if(err) 
            return cb(err);
        return cb();
    });
};

Collection.prototype.getItem = function(query, cb) {
    this.read((function(err, data) {
    if (query._id) 
         cb(data()[id]); // Use index
    else
        Object.keys(data).forEach(function(k) {
            var obj = data[k];
            Object.keys(query).forEach(function(qkey){
                if(obj[qkey] && (obj[qkey] === query[qkey])){
                    cb(obj);
                }
            });
        });
    }).bind(this));
};
Collection.prototype.getItems = function(cb) {
    this.read(function(error, data){
        var items = [];
        Object.keys(data).forEach(function(k){
            items.push(data[k]);
        });
        return cb(error, items);
    });
};

Collection.prototype.insertItem = function(item, cb){
    this.read((function(err, data) {
        var id = this.getNextId();
        var _item = {'_id':id};
        util._extend(_item, item); // Shallow copy
        data[id] = _item;
        this.insert(data, cb);
        this._last_id = id;
    }).bind(this));
};

