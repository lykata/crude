var Collection = require('./collection');

module.exports = Backend;

function Backend(){
    this.collection = new Collection('./test/users.json');
    this.uc = new UserController();
}
Backend.prototype.post_user = function(data, cb){
    if (this.uc.validate(data)){
        this.collection.insertItem(data, function(error){
            return cb(error);
        });
    }
};

Backend.prototype.get_user = function(data, cb){
    if (this.uc.validate(data))
        this.collection.getItem(data);
};

Backend.prototype.get_users = function(cb){
    return this.collection.getItems(cb);
};

function UserController() {
    this.fields = ['username'];
}

UserController.prototype.validate = function(data) {
    var valid = false;
    this.fields.some(function(k) {
        if (data[k]){
            valid = true;
        }
    });
    return valid;
};

