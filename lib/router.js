var Route = require('./route');
var util = require('util');

module.exports = Router;

var methods = ['get', 'post', 'patch' ];

function Router() {
    this.routes = [];
    this.error_handler = null;
}
Router.prototype.escape = function(string) {
    return string.replace(/\//g,'\\/');
};
Router.prototype.findRoute = function(url, method) {
    var error_handler;
    var rs = this.routes;
    method = method.toLowerCase();
    for (i=0; i< this.routes.length; i++){
        // Error handler found
        if (rs[i].method === 'error'){
            this.error_handler = rs[i];
            continue;
        }

        var url_pattern = this.escape(rs[i].url);

        var url_match = url
            .match(new RegExp(url_pattern));

        if (url_match && url_match[0] == url && method == rs[i].method){
            return rs[i];
        }
    }
};
Router.prototype.routeNotFound= function(req, res){
    var url = req.url;
    var method = req.method;
    res.statusCode = 404;
    res.statusMessage = util
        .format('No route for %s with method %s could be found \n', url, method);
    return res;
};

Router.prototype.handle = function(req, res) {
    var url = req.url;
    var method = req.method;
    var r = this.findRoute(url, method);
    if (r){
        console.log('Handling matching method for %s %s', url, method);
        r.handle(req, res);
    } else
        if (this.error_handler){
            res = this.routeNotFound(req, res);
            this.error_handler.handle(req, res);
        }
};
Router.prototype.route = function(method) {
    var fn;
    var url = null;
    var args = [].slice.call(arguments[1]);
    if (args.length == 2 ){
        url = args[0];
        fn = args[1];
    }else{
        fn = args[0];
    }
    r = new Route(method, fn, url);
    this.routes.push(r);
};




