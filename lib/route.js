module.exports = Route;

function Route(method, handle, url) {
    this.url = url;
    this.method = method;
    this._handle = handle;
}
Route.prototype.handle = function (req, res){
    if(req.headers['content-type'] == 'application/json'){
        var body = '';
        req.on('data', function(data) {
            body += data;
        });
        req.on('end', (function() {
            req.body = JSON.parse(body);
            return this._handle(req, res);
        }).bind(this));
    }else{
        return this._handle(req,res);
    }
};

