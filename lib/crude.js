var http = require('http');
var url = require('url');
var Router = require('./router');

var methods = ['get', 'post', 'patch', 'error' ];

module.exports = createApp;

function createApp() {
    var app = function(req, res) {
       router.handle(req, res);
    };
    var server = http.createServer(app);

    var router = new Router();
    methods.forEach(function(method){
        app[method] = function(path) {
            var route = router.route(method, arguments);
        };
    });

    app.listen = function(){
        return server.listen.apply(server, arguments);
    };
    app.close = function(){
        return server.close();
    };
    return app;
}
