var url = require('url');
var crude = require('./crude');
var Backend = require('./backend');

var backend = new Backend();

var app = crude();

app.get('/', function(req, res){
    res.writeHead(200);
    res.write('hello');
    res.end();
});
app.get('/users/.*', function(req, res){
    backend.get_users(function(error, users){
        res.writeHead(200);
        res.write(JSON.stringify(users));
        res.end();
    });
});
app.get('/users', function(req, res){
    backend.get_users(function(error, users){
        res.writeHead(200);
        res.write(JSON.stringify(users));
        res.end();
    });
});
app.post('/users', function(req, res){
    backend.post_user(req.body,function(){
        res.writeHead(201, {'Content-Type': 'plain/text'});
        res.end();
    });
});

app.error(function(req, res) {
    res.write(res.statusMessage);
    res.end();
});

exports.listen = function(port){
    app.listen(port);
    console.log('Listening on port %s', 1332);
};
exports.close = function(port){
    app.close();
};

